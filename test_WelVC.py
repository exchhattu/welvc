from HW_WelVC import add_two_numbers


def test_add_two_numbers():
    assert add_two_numbers(5, 10) == 15
    assert add_two_numbers(5, 10.5) == 15.5
