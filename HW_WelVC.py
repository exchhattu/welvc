#/usr/local/bin/python3

def print_hello_world():
    print("Hello World, Welcome to GitHub Vesion Control")

def add_two_numbers(num1, num2):
    return num1+num2



if __name__ == "__main__":
    # print hello world
    print_hello_world()

    # sum two numbers
    num1 = 12
    num2 = 14
    print("Sum of %d and %d is %d " %(num1, num2, add_two_numbers(num1, num2)))
