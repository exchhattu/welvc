# base image
# FROM python:3
FROM python:2

# Add essential script to the Dockerfile
ADD HW_WelVC.py /script/

# Execute the command when image loads
# CMD [ "python3", "./script/HW_WelVC.py" ]
CMD [ "python2", "./script/HW_WelVC.py" ]
