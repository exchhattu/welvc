# Welcome to Git version control

Welcome to git - minimal survival toolkit.

## Installation
* Install git in your computer, if you do not have

## Create a repo in GitLab, GitHub, or bitbucket
* Goto GitLab website (https://gitlab.com)
* Create your account if you do not have 
* Create your first repo! (either private or public)

__I created repo named welvc in my account and will show steps using it.__

## Clone
```
$ git clone https://gitlab.com/exchhattu/welvc.git
```

## Add new file
1. Create a file - use your own favorite editor
```
$ vi ./HW_WelVC.py
```

2. Write a few lines of code in ./HW_WelVC.py
```
#/usr/local/bin/python3

def print_hello_world():
    print("Hello World, Welcome to GitHub Vesion Control")

def add_two_numbers(num1, num2):
    return num1+num2

if __name__ == "__main__":
    # print hello world
    print_hello_world()

    # sum two numbers
    num1 = 12
    num2 = 14
    print("Sum of %d and %d is %d " %(num1, num2, add_two_numbers(num1, num2)))
    print("Hello World, Welcome to GitHub Vesion Control")
```

3. Add a file in git 
```
$ git add ./HW_WelVC.py
```

4. Commit - once your added your change to git, commit it with meaningful message
```
$ git commit -m "add a new python file" 

-m indicates message for your commit. This should summarize why do you change your code.

If you get confuse with syntax, please get a help using following command

$ git help commit
```

4. Push to server - push your change to a server (in this case gitlab). 
Otherwise, it will stay locally. You can only have access within your 
computer. 

```
$ git push origin master
```

5. Check your github account - you will see a change!


## Advance (track mistakes automatically) 
### CI/CD - continuous integration and continuous deployment 

#### CICD setup 
Install gitlab-runner if CICD from gitlab would be used
Please go to https://docs.gitlab.com/ee/ci/ for detail 

* Installation in mac
```
$ brew install gitlab-runner
$ brew services start gitlab-runner
$ gitlab-runner stop
```

#### Prepare files - maintainer or owner of repos
* Steps 
1. Docker container - please look my Dockerfile (name is always Dockerfile)
For detail, you can click the linke below. 
https://docs.docker.com/develop/develop-images/dockerfile_best-practices/

2. Create .gitlab-ci.yml (look if in repo) at root directory of repos 
For more detail, please click https://docs.gitlab.com/ee/ci/yaml/

3. Build
* Based on the commands written in two files, pipeline can be created for 
automation - build, test, and deploy. 

### Docker 
1. Build docker image (container) and test it  
```
$ docker build -t registry.gitlab.com/exchhattu/welvc .
```

2. Push to gitlab for deployment  
```
$ docker push registry.gitlab.com/exchhattu/welvc 
```

3. Pull from gitlab and run it
```
$ docker pull registry.gitlab.com/exchhattu/welvc
$ docker run registry.gitlab.com/exchhattu/welvc

output
Hello World, Welcome to GitHub Vesion Control
Sum of 12 and 14 is 26 
```